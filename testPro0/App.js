/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';



const App = () => {
  

  return (
    <View style={styles.ContainerZero}>
      <View style={styles.ContainerOne}>
        <View style={{flexDirection:'row', justifyContent:'space-between', margin: 20,}}>
          <Text style={{fontSize: 30, fontWeight:'bold', color:'#081a28'}}> ← </Text>
          <Text style={{fontSize: 30, fontWeight:'bold', color:'#081a28'}}> : </Text>
        </View>
        <Text style={{fontSize: 25, fontWeight:'900', marginTop: 15, color:'#081a28', marginLeft: 30,}}>My Profile</Text>
      <TouchableOpacity>
        
        <Image
          style={{
             
            width: 80,
            height: 80,
            justifyContent:'center',
            alignSelf:'center',
            borderRadius: 50,
            marginTop: 20,
            
          }}
        source={{
          uri: 'https://i.pinimg.com/originals/27/b2/16/27b216fa373d75906c2b8b51661d8b13.gif',
        }}
         />
         
         <Text style={{textAlign:'center', fontSize:25, fontWeight:'900', marginTop: 10, color:'#081a28'}}> Zeref Darniel</Text>
         </TouchableOpacity>
         <View style={{justifyContent:'center', alignItems:'center', marginTop: 10}}>
         <Text style={{fontSize: 18, fontWeight:'600', color:'#aaaaaa' }}> summa oru bio for now </Text>
         <Text style={{fontSize: 18, fontWeight:'600', color:'#aaaaaa', marginTop: 4 }}> Earth - ER08Z34 </Text>
         </View>
         <View style={{flexDirection:'row', justifyContent:'space-evenly', marginTop: 20,}}>
              <Text style={{fontSize: 18, fontWeight:'400', color:'#aaaaaa' }}>Photos</Text>
              <Text style={{fontSize: 18, fontWeight:'400', color:'#aaaaaa' }}>Followers</Text>
              <Text style={{fontSize: 18, fontWeight:'400', color:'#aaaaaa' }}>Posts</Text>
         </View>
         <View style={{flexDirection:'row', justifyContent:'space-evenly'}}>
              <Text style={{fontSize: 18, fontWeight:'900', color:'#081a28' }}>780</Text>
              <Text style={{fontSize: 18, fontWeight:'900', color:'#081a28',marginLeft: 10, }}>1028</Text>
              <Text style={{fontSize: 18, fontWeight:'900', color:'#081a28',marginLeft: 20, }}>182</Text>
         </View>
      </View>
      <View style={{flex: 0.4,}}>
        <View style={{flex: 1, margin: 20, justifyContent:'center', alignItems:'center', flexDirection:'row', }}>
      <Image
          style={styles.BottomImages}
        source={{
          uri: 'https://media.istockphoto.com/videos/following-shot-of-female-astronaut-in-space-suit-confidently-walking-video-id933634368?b=1&k=20&m=933634368&s=640x640&h=nDUivbl1J8k5ukZlh9TZ6CeI6x7knvAPjl0tC4NJNIg=',
        }}
         />
         <View style={{flexDirection:'column'}} >
         <Image
          style={{width: 150, height: 120,borderRadius: 10, marginBottom: 10,}}
        source={{
          uri: 'https://i0.wp.com/indigo.careers/wp-content/uploads/2019/08/Dream-Jobs-Astronaut-2400x1800-1.jpg?fit=2400%2C1800&ssl=1',
        }}
         /> 
          <Image
          style={{width: 150, height: 120,borderRadius: 10,}}
        source={{
          uri: 'https://www.orfonline.org/wp-content/uploads/2021/07/astron.jpg',
        }}
         /> 
         </View>
      </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
 ContainerZero: {
    flex: 1,
 },
 ContainerOne: {
   flex: 0.6,
   backgroundColor:'#fff',
   borderBottomLeftRadius: 25,
   borderBottomRightRadius: 25,
   elevation: 15,  
 },
 BottomImages: {
   width: 170,
   height: 250,
   borderRadius: 10,
   marginRight: 10,
 }
});

export default App;
