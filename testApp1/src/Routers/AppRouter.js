import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import RootStackScreen from './RootStackScreen';
import {AuthContext} from './AuthProvider';

const AppRouter = () => {

    //  const {user, setUser} = useContext(AuthContext);
    //  const [initializing, setInitializing] = useState(true);
    return (
        <NavigationContainer>
            <RootStackScreen />
        </NavigationContainer>
    )
}

export default AppRouter
