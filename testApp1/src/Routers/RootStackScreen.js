import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import LoginScreen from '../LoginScreen'
import SignUpScreen from '../SignUpScreen'
import HomeScreen from '../HomeScreen'
import ScrewDriver from '../screens/ScrewDriver'
import NotScrewDriver from '../NotScrewDriver'

const RootStack = createStackNavigator();

const RootStackScreen = () => {
    return (
        <RootStack.Navigator >
        <RootStack.Screen name = 'HomeScreen' component={HomeScreen} />
        <RootStack.Screen name = 'LoginScreen' component={LoginScreen} />
        <RootStack.Screen name = 'SignUpScreen' component={SignUpScreen} />
        <RootStack.Screen name = 'ScrewDriver' component={ScrewDriver} />
        <RootStack.Screen name = 'NotScrewDriver' component={NotScrewDriver} />
        </RootStack.Navigator>
    )
}

export default RootStackScreen
