import React,{useState} from 'react'
import { View, Text, TextInput, TouchableOpacity } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Profile from './screens/Profile';
import ScrewDriver from './screens/ScrewDriver';
import Riger from './screens/Riger';

const Tab = createMaterialBottomTabNavigator();

const NotScrewDriver = () => {

    // const setData = async () => {
    //     await AsyncStorage.setItem('dabe', 'varuthu')
    // }
    // const getData = async () => {
    //    const valo =  await AsyncStorage.getItem('dabe')
    //    console.log(valo)
    // }

    // const [Yoru, setYoru] = useState('')
    // const [Brim, setBrim] = useState('')
    // const saveJett = () => {
    //     if (Yoru){
    //         AsyncStorage.setItem('any_key_here', 'it working');
    //         setYoru('');
    //         alert('data kadachichu 🥳');
    //     }
    //     else{
    //         alert('ethavathu type panu bro 😐');
    //         AsyncStorage.setItem('any_key_here', 'dabe');
    //     }
    // }

    // const getJett = () => {
    //     AsyncStorage.getItem('any_key_here')
    //     .then((Brim) => {
    //         alert('heyy', Brim)
    //         setBrim(Brim);
    //     })
    // }

    return (
        <Tab.Navigator
        initialRouteName="Feed"
        activeColor="#3533f5"
        barStyle={{ backgroundColor: '#9973fa' }}
      >
        <Tab.Screen
          name="Profile"
          component={Profile}
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="home" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="Notifications"
          component={ScrewDriver}
          options={{
            tabBarLabel: 'Updates',
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="bell" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="test R"
          component={Riger}
          options={{
            tabBarLabel: 'Profile',
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="account" color={color} size={26} />
            ),
          }}
        />
      </Tab.Navigator>
    )
}

export default NotScrewDriver
