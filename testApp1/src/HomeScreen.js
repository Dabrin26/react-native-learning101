import { NavigationContainer } from '@react-navigation/native'
import React,{useEffect} from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, ScrollView, } from 'react-native'
import SplashScreen from 'react-native-splash-screen'
// import LinearGradient from 'react-native-linear-gradient';

const HomeScreen = ({navigation}) => {
    useEffect(() => {
        SplashScreen.hide(); 
    }, [])
    return (
        <View style={styles.container}>
            <ScrollView>
        <View style={{ alignItems:'flex-start',justifyContent:'flex-start', marginLeft: 10,}}>
        
        <Text style={{justifyContent:'flex-start', marginTop: 10, fontSize: 25,}}>SLOPE</Text>
        
        </View>
        <View>
        <Image  source={{
          uri: 'https://media.istockphoto.com/vectors/mom-freelancer-works-at-home-vector-id1095111248?k=20&m=1095111248&s=612x612&w=0&h=T7X-AEEW6t1mZfKOe9k4hHv8rOIgm7mWiZW6ZwCaQnA=',
        }} style={styles.img0}/>
        </View>
        <View style={{justifyContent:'center', alignItems:'center',}}>
            <Text style={{fontSize:40, marginTop: 30, fontWeight:'900'}}> Hello !</Text>
            <Text style={{margin: 20, marginTop: 10, textAlign:'center', }}> Since i couldn't see the text in the given example clearly i had to come up with something on my own</Text>
        </View>
        <View style={{marginTop: 20}}>
           <TouchableOpacity onPress={ () => navigation.navigate('LoginScreen')}>
            {/* <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']}> */}
                <View 
                style={{
                    justifyContent:'center',
                    alignItems:'center',
                    alignContent:'center',
                    width: 300,
                    height: 50,
                    marginHorizontal: 55,
                    backgroundColor:'#3533f5',
                    borderRadius: 4,
                    
            }}
                >
                    <Text style={{color:'#fff', fontWeight:'700', fontSize: 25}}>LOGIN</Text>
                </View>
                {/* </LinearGradient> */}
                </TouchableOpacity>
                <TouchableOpacity onPress={ () => navigation.navigate('SignUpScreen')}>
                <View 
                style={{
                    justifyContent:'center',
                    alignItems:'center',
                    alignContent:'center',
                    width: 300,
                    height: 50,
                    marginHorizontal: 55,
                    backgroundColor:'#3533f5',
                    borderRadius: 4,
                    marginTop: 30,
            }}
                >
                    <Text style={{color:'#fff', fontWeight:'700',fontSize: 25}}>SIGN UP</Text>
                </View>
               
           </TouchableOpacity>
           
        </View>
        </ScrollView>
    </View>
         
    );
  };
  
  const styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:'#fff'
      
  },
  img0:{
      alignItems: 'center',
      justifyContent:'center',
      width: 300,
      height: 240,
      marginTop: 60,
      marginHorizontal: 60,
  },
  
    
  });

export default HomeScreen
