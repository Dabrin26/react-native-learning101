import React,{useState} from 'react'
import { View, Text, StyleSheet,TextInput,onChangeText,text, TouchableOpacity,Image, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import auth from '@react-native-firebase/auth';

const SignUpScreen = ({navigation}) => {
    const [Name, setName] = useState('')
    const [Email, setEmail] = useState('')
    const [Pass, setPass] = useState('')
    const validate = () => {
        if(Name === ''){
            Alert.alert('bro peru kudu bro 😬')
        }
        else if(Email === ''){
            Alert.alert('bro email kudu bro 😬')
        }
        else if (Pass === ''){
            Alert.alert(' password enga ... 😐?')
        } else {
            auth()
  .createUserWithEmailAndPassword(Email, Pass)
  .then(() => {
    console.log('User account created & signed in!');
  })
  .catch(error => {
    if (error.code === 'auth/email-already-in-use') {
      console.log('That email address is already in use!');
    }

    if (error.code === 'auth/invalid-email') {
      console.log('That email address is invalid!');
    }

    console.error(error);
  });
        }
    }
    return (
        <View>
            <KeyboardAwareScrollView>
        <View>
            <Text 
            style={{
                          marginStart: 30, 
                          fontSize: 45, 
                          fontWeight:'bold', 
                          color:'#000',
                          marginTop: 50,
                          }}> Hi !
            </Text>
            <Text style={{
                          marginStart: 36, 
                          fontSize: 22,
                          color:'#000',
                          marginVertical: 15,
        }}>Create a new account </Text>
        </View>
        <View  style={{
            marginVertical: 25,
            justifyContent:'center',
            alignItems:'center',
                }}>
                    <TextInput
        style={{
            color:'#000',
            marginTop: 25,
            fontSize: 20,
            width: 300,
            borderBottomWidth : 2,
            justifyContent:'center',
            alignItems:'center',
                }}
        onChangeText={(text) => setName(text)}
        placeholder="Enter your Name 😉"
        value={text}
        
      />

        <TextInput
        style={{
            color:'#000',
            marginTop: 25,
            fontSize: 20,
            width: 300,
            borderBottomWidth : 2,
            justifyContent:'center',
            alignItems:'center',
                }}
        onChangeText={(text) => setEmail(text)}
        placeholder="Enter your Email 👻"
        value={text}
        
      />

        <TextInput
        style={{
            color:'#000',
            marginTop: 30,
            fontSize: 20,
            width: 300,
            borderBottomWidth : 2,
            justifyContent:'center',
            alignItems:'center',
            // textAlign:'center',
                }}
        onChangeText={(text) => setPass(text)}
        secureTextEntry={true}
        placeholder="Enter your password "
        value={text}
        
      />
        </View>

            <View style={{
                marginTop: 30,
            }}>
                <TouchableOpacity onPress= {validate}>
        <View 
                style={{
                    justifyContent:'center',
                    alignItems:'center',
                    alignContent:'center',
                    width: 300,
                    height: 50,
                    marginHorizontal: 55,
                    backgroundColor:'#3533f5',
                    borderRadius: 4,
                    
            }}
                >
                    <Text style={{color:'#fff', fontWeight:'700', fontSize: 25}}>SIGN UP</Text>
                </View>
            </TouchableOpacity>

            {/* <Text style={{
                marginTop: 10,
                color:'#9973fa',
                justifyContent:'center',
                textAlign:'center',
                }}>
                Forgot Password ?
            </Text> */}
                </View>

                <View style ={{ justifyContent:'center', alignItems:'center'}}>
                    <Text style={{fontWeight:'900', fontSize:25, marginTop: 10,}}> ----------- or -----------  </Text>
                </View>

                <View style={{
                    justifyContent:'center',
                    alignItems:'center',
                }}>
                    <Text style={{
                        marginTop: 15,
                        fontWeight:'bold',
                        fontSize: 18,
                        color:'#000',
                }}>Social Media Login </Text>
                </View>

                <View style={{
                    marginTop: 5,
                    flexDirection:'row',
                    alignItems:'center',
                    justifyContent:'center'
                    
                }}>
               <Icon name="google" size={50} style={{ marginRight: 40, color:'#abe625' }} />
                <Icon name="facebook-square" size={50} style={{ marginRight: 40, color:'#2fa4f1' }} />
                <Icon name="apple" size={50} style={{ marginRight: 10, color:'#8c8c8c' }} />


                </View>
                <View style={{
                    flexDirection:'row',
                    justifyContent:'center',
                    alignItems:'center',
                }}>
                <Text> Already have an account ?</Text>
                <TouchableOpacity onPress={ () => navigation.navigate('LoginScreen')}>
                <Text style={{fontWeight:"900",color:'#000', marginVertical:10,}}>  Sign in!</Text>
                </TouchableOpacity>
                </View>

                </KeyboardAwareScrollView>

        </View>
    )
}

export default SignUpScreen
