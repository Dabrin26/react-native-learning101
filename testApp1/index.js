/**
 * @format
 */

import {AppRegistry} from 'react-native';
import AppRouter from './src/Routers/AppRouter';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => AppRouter);
