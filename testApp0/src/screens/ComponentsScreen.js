import React from "react";
import {Text,
        View,
     StyleSheet,
     TouchableOpacity,
     Button,
    } from 'react-native';


const ComponentsScreen = () => {
    return (
    
   
        <View style={styles.container}>
        <View style={{ alignItems:'flex-start',justifyContent:'flex-start', marginLeft: 20,}}>
        
        <Text style={{justifyContent:'flex-start', marginTop: 30, fontSize: 25,}}>SLOPE</Text>
        
        </View>
        <View>
        {/* <Image source={require('../testApp0/assets/imgUn.png')} style={styles.img0}/> */}
        </View>
        <View style={{justifyContent:'center', alignItems:'center',}}>
            <Text style={{fontSize:40, marginTop: 40, fontWeight:'900'}}> Hello !</Text>
            <Text style={{margin: 20, marginTop: 20, textAlign:'center', }}> Since i couldn't see the text in the given example clearly i had to come up with something on my own</Text>
        </View>
        <View style={{marginTop: 40}}>
           <TouchableOpacity>
                <View 
                style={{
                    justifyContent:'center',
                    alignItems:'center',
                    alignContent:'center',
                    width: 300,
                    height: 50,
                    marginHorizontal: 55,
                    backgroundColor:'#0D3AA9',
                    
            }}
                >
                    <Text style={{color:'#fff', fontWeight:'700', fontSize: 25}}>LOGIN</Text>
                </View>
  
                <View 
                style={{
                    justifyContent:'center',
                    alignItems:'center',
                    alignContent:'center',
                    width: 300,
                    height: 50,
                    marginHorizontal: 55,
                    backgroundColor:'#0D3AA9',
                    marginTop: 30,
            }}
                >
                    <Text style={{color:'#fff', fontWeight:'700',fontSize: 25}}>SIGN UP</Text>
                </View>
               
           </TouchableOpacity>
           
        </View>
    </View>
         
    );
  };
  
  const styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:'#fff'
      
  },
  img0:{
      alignItems: 'center',
      justifyContent:'center',
      width: 300,
      height: 240,
      marginTop: 60,
      marginHorizontal: 60,
  },
  
    
  });
  
export default ComponentsScreen;
