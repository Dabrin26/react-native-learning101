/**
 * @format
 */

import {AppRegistry} from 'react-native';
import HomeScreenUn from './src/HomeScreenUn';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => HomeScreenUn);
