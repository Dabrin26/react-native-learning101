/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  useState,
  counter
} from "react";
// made by dab
import {
  StyleSheet,
  Text,
  View,
  Linking,
  Button,
  ScrollView,
} from 'react-native';


const App = () => {

  const [Items, setItems] = useState([
    {key:1, item:'item 1'},
    {key:2, item:'item 2'},
    {key:3, item:'item 3'},
    {key:4, item:'item 4'},
    {key:5, item:'item 5'},
    {key:6, item:'item 6'},
    {key:7, item:'item 7'},
    {key:8, item:'item 8'},
    {key:9, item:'item 9'},
    {key:10, item:'item 10'},
    {key:11,item:'item 11'},
    {key:12,item:'item 12'},
    {key:13, item:'item 13'},
    {key:14,item:'item 14'},
    {key:15,item:'item 15'},
  ])
  return(
   <View style={styles.body}>
     <ScrollView horizontal={true}>
     {
       Items.map((item)=> {
         return(
          <View style={styles.item}>
          <Text style={styles.text}> {item.item} </Text>
          </View>
         )
       })
     }
     </ScrollView>
   </View>
  );
};


const styles = StyleSheet.create({
  body: {
    flex: 1,
    flexDirection:'row',
    // #98ff9f  #147371
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
    // borderRadius: 10,
    // margin: 30,
  },
  item:{
    
    backgroundColor:'#98ff9f',
    height: 70,
    margin: 10,
    alignItems: 'center',
  },
  text:{
    color:'#724499',
    fontSize: 35,
    
  },
  Button:{
    width: 150,
    height: 40,
    backgroundColor:'#98ff9f',
    marginBottom: 10,
  }
});

export default App;
