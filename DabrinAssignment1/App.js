 import React from 'react';
 import Svg,{Path, Rect,}from 'react-native-svg';
 import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Dimensions,
  View,
  Image,
} from 'react-native';

const {width, height} = Dimensions.get('screen');

const App = () => {

  const charWidth = width -0;
  const charHeight = height /3;

  const start =  `${charWidth} ${charHeight /1}`;
  const controlPointA = `${charWidth/3} ${charHeight /20}`;
  const controlPointB = `${(charWidth/5) * 3} ${charHeight}`;
  const end = `0, ${charHeight}`; 

  return (
   <View style={styles.Container}>
     <View>
       <Svg width={charWidth} height={charHeight}>
        <Rect width={charWidth} height={charHeight} fill={'#F89880'}
        />
          <Path 
          d={`M${start} C${controlPointA} ${controlPointB} ${end} v${end}`}
          stroke={'#fff'}
          fill={'#fff'}
          strokeWidth={3}
          />
       </Svg>
       <View style={styles.Container0}>
            <View style={styles.Container1}>
              <Text style={styles.Text0}>
                  Good Morning
              </Text>
              <Text style={styles.Text1}>
                  Saatvik Phacino
              </Text>
            </View>
            <View style={styles.Container2}>
              <Icon name="cloud" size={40} color="#fff" />
              <Text style={styles.Text2}>21c  </Text>
              <Text style={styles.Text3}>cloudy</Text>
            </View>
          </View>
     </View>
     <View style={styles.Container3}>
      <Text style={styles.Text4}>Start a journey</Text>
      <ScrollView style={styles.Container4}>
        <View style={styles.Container5}>
        <View style={styles.Container6}>
            <View style={styles.Container7}>
              <Text style={styles.TextCard}>Walking</Text>
              <Icon name="walking" size={40} color="#F89880" style={styles.IconCard}/>
            </View>
             <View style={styles.Container7}>
              <Text style={styles.TextCard}>Cycling</Text>
              <Icon name="bicycle" size={40} color="#F89880" style={styles.IconCard}/>
            </View>
        </View>
        <View style={styles.Container6}>
            <View style={styles.Container7}>
              <Text style={styles.TextCard}>Driving</Text>
              <Icon name="car-alt" size={40} color="#F89880" style={styles.IconCard}/>
            </View>
             <View style={styles.Container7}>
              <Text style={styles.TextCard}>Train</Text>
              <Icon name="train" size={40} color="#F89880" style={styles.IconCard}/>
            </View>
        </View>
        <View style={styles.Container6}>
            <View style={styles.Container7}>
              <Text style={styles.TextCard}>Hiking</Text>
              <Icon name="hiking" size={40} color="#F89880" style={styles.IconCard}/>
            </View>
             <View style={styles.Container7}>
              <Text style={styles.TextCard}>Flight</Text>
              <Icon name="plane-departure" size={40} color="#F89880" style={styles.IconCard}/>
            </View>
        </View>
        </View>
      </ScrollView>
     </View>

     {/* <View style={{position:'absolute', bottom: 0, backgroundColor:'rgba(52, 52, 52, 0.2)', height: 80, flexDirection:'row',justifyContent:'space-around',}}>
     <Icon name="home" size={40} color="#F89880" style={{marginRight: 40, marginLeft:40, marginVertical: 15,}}/>
     <Icon name="repeat" size={40} color="#696969" style={{marginRight: 40, marginVertical: 15}}/>
     <Image source={{uri: 'https://i.pinimg.com/originals/85/9a/f7/859af748d1eed0d67d5801a6df188a89.jpg'}}
       style={{width: 40, height: 40, borderRadius: 50,marginRight: 40, marginVertical: 15}} />
     <Icon name="bar-chart" size={40} color="#696969" style={{marginRight: 40, marginVertical: 15}}/>
     <Icon name="bell-o" size={40} color="#696969" style={{marginRight: 40, marginVertical: 15}}/>
     </View> */}
   </View>
  );
};

const styles = StyleSheet.create({
  // C O N T A I N E R 
  Container: 
  {flex: 1,backgroundColor:'#fff'},
  Container0:
  {flexDirection:'row', zIndex: 99, marginVertical: -180,},
  Container1:
  {flexDirection:'column', marginHorizontal: 15,},
  Container2:
  {justifyContent:'flex-end', alignItems:'flex-end', marginHorizontal: 55,},
  Container3:
  {marginVertical: 260,},
  Container4:
  {height: 340,},
  Container5:
  {justifyContent: 'center', alignItems:'center'},
  Container6:
  {flexDirection:'row', backgroundColor:'#fff'},
  Container7:
  {width: 180, height: 150, borderRadius: 20, elevation: 5, backgroundColor:'#fff', margin: 10,},
  // T E X T 
  Text0:
    {color:'#fff', fontSize: 18, fontWeight: 'bold'},
  Text1:
  {color:'#fff', fontSize: 28, fontWeight: 'bold'},
  Text2:
  {color:'#fff', fontWeight:'bold', fontSize: 16},
  Text3:
  {color:'#fff'},
  Text4:
  {fontSize: 22, fontWeight: 'bold', marginBottom: 20, marginHorizontal: 30,},
  TextCard:
  {zIndex: 999,marginTop: 30, marginLeft: 30, fontSize:24, fontWeight:'bold'},
  IconCard:{
    marginLeft: 110,
    marginVertical: 20,
  }
});

export default App;
